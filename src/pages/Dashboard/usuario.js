import React, { Fragment } from 'react';

function Usuario() {
    return (
        <Fragment>
            <div className="app-page-title">
                <div className="page-title-wrapper">
                    <div className="page-title-heading">
                        <div className="page-title-icon">
                            <i className="pe-7s-users icon-gradient bg-mean-fruit">
                            </i>
                        </div>
                        <div>Usuários
                        <div className="page-title-subheading">
                                Página Principal
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="main-card mb-3 card">
                        <div className="card-body">
                            <table className="mb-0 table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Coluna 1</th>
                                        <th>Coluna 2</th>
                                        <th>Coluna 3</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>remaining essentially unchanged</td>
                                        <td>Otto</td>
                                        <td>@ and more recently </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Hampden-Sydney College</td>
                                        <td>Thornton</td>
                                        <td>@first line of Lorem </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>randomised words which</td>
                                        <td>the Bird</td>
                                        <td>@ Ipsum used since </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Usuario;