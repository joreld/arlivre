import React, { Fragment } from 'react';

function Corrida() {
    return (
        <Fragment>
            <div className="app-page-title">
                <div className="page-title-wrapper">
                    <div className="page-title-heading">
                        <div className="page-title-icon">
                            <i className="pe-7s-pin icon-gradient bg-mean-fruit">
                            </i>
                        </div>
                        <div>Histórico de Corridas
                        <div className="page-title-subheading">
                                Página Principal
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="main-card mb-3 card">
                        <div className="card-body">
                            <table className="mb-0 table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Email Usuário</th>
                                        <th>Data</th>
                                        <th>Horário</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>email1@gmail.com</td>
                                        <td>10/12/2019</td>
                                        <td>15:48 às 16:10</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>email2@gmail.com</td>
                                        <td>20/01/2020</td>
                                        <td>08:05 às 08:20</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>email3@gmail.com</td>
                                        <td>21/01/2020</td>
                                        <td>09:40 às 10:20</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Corrida;