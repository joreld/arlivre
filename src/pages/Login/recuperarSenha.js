import React from 'react';

function RecuperarSenha() {
    return (
        <div className="modal-content">
            <div className="modal-header">
                <div className="h5 modal-title">Recuperar senha?<h6 className="mt-1 mb-0 opacity-8"><span>Use o campo abaixo para recuperar.</span></h6></div>
            </div>
            <div className="modal-body">
                <div>
                    <form className="">
                        <div className="form-row">
                            <div className="col-md-12">
                                <div className="position-relative form-group">
                                    <label for="exampleEmail" className="">Email</label>
                                    <input name="email" id="exampleEmail" placeholder="Email " type="email" className="form-control" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="divider"></div>
                <h6 className="mb-0"><a href="/auth/login" className="text-primary">Formulário de Login</a></h6></div>
            <div className="modal-footer clearfix">
                <div className="float-right">
                    <button className="btn btn-primary btn-lg">Recuperar Senha</button>
                </div>
            </div>
        </div>
    );
}

export default RecuperarSenha;