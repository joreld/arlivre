import React from 'react';
import { useForm } from 'react-hook-form';

function Login() {

    const { register, handleSubmit, errors } = useForm();


    const onSubmit = (data) => {
        if(data.email === "teste@teste.com" && data.senha === "123123")
           window.location.href = "/dash/home";
    };

    return (
        <div className="modal-content">
            <div className="modal-body">
                <div className="h5 modal-title text-center">
                    <h4 className="mt-2">
                        <div>Bem-vindo,</div>
                        <span>Entre com as credenciais para ter acesso.</span>
                    </h4>
                </div>
                <div className="divider"></div>
                <form className={errors.senha || errors.email ? 'needs-validation was-validated' : ''} noValidate onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row">
                        <div className="col-md-12">
                            <div className="position-relative form-group">
                                <input ref={register({
                                    required: true,
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
                                    }
                                })} name="email" autoComplete="username" placeholder="Email" type="email" className="form-control" required />
                                {errors.email && errors.email.type === "required" && <div className="invalid-feedback">O campo E-mail é obrigatório!</div>}
                                {errors.email && errors.email.type === "pattern" && <div className="invalid-feedback">Informe um E-mail válido!</div>}
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="position-relative form-group">
                                <input ref={register({ required: true })} name="senha" placeholder="Senha" type="password" className="form-control" required />
                                {errors.senha && errors.senha.type === "required" && <div className="invalid-feedback">O campo Senha é obrigatório!</div>}
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer clearfix">
                        <div className="float-right">
                            <button type="submit" className="btn btn-primary btn-lg">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Login;