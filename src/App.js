import React from 'react';

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import LoginLayout from './layouts/login';
import DashLayout from './layouts/dashboard';

function App() {
  return (
    <BrowserRouter>
      <Switch>
      <Route path="/dash" render={props => <DashLayout {...props} />} />
        <Route path="/auth" render={props => <LoginLayout {...props} />} />
        <Redirect from="/" to="/auth/login" />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
