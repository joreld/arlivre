import React, { Fragment } from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import LoginRoutes from '../routes/login';

function Login(props) {

    function getRoutes(routes) {
        if (props.match.url === "/auth/") {
            return <Redirect to="/auth/login" />
        } else {
            return routes.map((prop, key) => {
                if (prop.layout === "/auth") {
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            component={prop.component}
                            key={key}
                        />
                    );
                } else {
                    return null;
                }
            });
        }
    }

    return (
        <Fragment>
            <div className="app-container app-theme-white body-tabs-shadow">
                <div className="app-container">
                    <div className="h-100 bg-plum-plate bg-animation">
                        <div className="d-flex h-100 justify-content-center align-items-center">
                            <div className="mx-auto app-login-box col-md-8">
                                <div className="app-logo-inverse mx-auto mb-3"></div>
                                <div className="modal-dialog w-100 mx-auto">
                                    <Switch>
                                        {getRoutes(LoginRoutes)}
                                    </Switch>
                                </div>
                                <div className="text-center text-white opacity-8 mt-3">Copyright © BackOffice</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    );
}

export default Login;