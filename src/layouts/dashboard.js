import React, { Fragment } from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import DashRoutes from '../routes/dashboard';

function Dashboard(props) {

    function getRoutes(routes) {
        if (props.match.url === "/dash/") {
            return <Redirect to="/dash/home" />
        } else {
            return routes.map((prop, key) => {
                if (prop.layout === "/dash") {
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            component={prop.component}
                            key={key}
                        />
                    );
                } else {
                    return null;
                }
            });
        }
    }

    return (
        <Fragment>
            <div className="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
                <div className="app-header header-shadow">
                    <div className="app-header__logo">
                        <div className="header__pane ml-auto">
                            <div>
                                <button type="button" className="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span className="hamburger-box">
                                        <span className="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="app-header__mobile-menu">
                        <div>
                            <button type="button" className="hamburger hamburger--elastic mobile-toggle-nav">
                                <span className="hamburger-box">
                                    <span className="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div className="app-header__menu">
                        <span>
                            <button type="button"
                                className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span className="btn-icon-wrapper">
                                    <i className="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>
                </div>
                <div className="app-main">
                    <div className="app-sidebar sidebar-shadow">
                        <div className="app-header__logo">
                            <div className="header__pane ml-auto">
                                <div>
                                    <button type="button" className="hamburger close-sidebar-btn hamburger--elastic"
                                        data-class="closed-sidebar">
                                        <span className="hamburger-box">
                                            <span className="hamburger-inner"></span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="app-header__mobile-menu">
                            <div>
                                <button type="button" className="hamburger hamburger--elastic mobile-toggle-nav">
                                    <span className="hamburger-box">
                                        <span className="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div className="app-header__menu">
                            <span>
                                <button type="button"
                                    className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                    <span className="btn-icon-wrapper">
                                        <i className="fa fa-ellipsis-v fa-w-6"></i>
                                    </span>
                                </button>
                            </span>
                        </div>
                        <div className="scrollbar-sidebar">
                            <div className="app-sidebar__inner">
                                <ul className="vertical-nav-menu">
                                    <li className="app-sidebar__heading">Dashboard</li>
                                    <li>
                                        <a href="/dash/home" className="mm-active">
                                            <i className="metismenu-icon pe-7s-home"></i>
                                            Home
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/dash/pagamento">
                                            <i className="metismenu-icon pe-7s-culture"></i>
                                            Pagamentos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/dash/patinete">
                                            <i className="metismenu-icon pe-7s-bicycle"></i>
                                            Patinetes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/dash/corrida">
                                            <i className="metismenu-icon  pe-7s-pin"></i>
                                            Corridas
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/dash/usuario">
                                            <i className="metismenu-icon pe-7s-users"></i>
                                            Usuários
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="app-main__outer">
                        <div className="app-main__inner">
                            <Switch>
                                {getRoutes(DashRoutes)}
                            </Switch>
                        </div>
                        <div className="app-wrapper-footer">
                            <div className="app-footer">
                                <div className="app-footer__inner">
                                    <div className="app-footer-left">.
                                    </div>
                                    <div className="app-footer-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    );
}

export default Dashboard;