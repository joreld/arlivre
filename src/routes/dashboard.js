import HomePage from '../pages/Dashboard/home';
import PagamentoPage from '../pages/Dashboard/pagamentos';
import PatinetePage from '../pages/Dashboard/Patinetes/patinetes';
import CorridaPage from '../pages/Dashboard/corridas';
import UsuarioPage from '../pages/Dashboard/usuario';

var dash = [
  {
    path: "/home",
    layout: "/dash",
    name: "Home",
    component: HomePage
  },
  {
    path: "/pagamento",
    layout: "/dash",
    name: "Pagamento",
    component: PagamentoPage
  },
  {
    path: "/patinete",
    layout: "/dash",
    name: "Patinete",
    component: PatinetePage
  },
  {
    path: "/corrida",
    layout: "/dash",
    name: "Corrida",
    component: CorridaPage
  },
  {
    path: "/usuario",
    layout: "/dash",
    name: "Usuario",
    component: UsuarioPage
  }
];
export default dash;