import LoginPage from '../pages/Login/login';
import RecuperarSenhaPage from '../pages/Login/recuperarSenha';

var login = [
  {
    path: "/login",
    layout: "/auth",
    name: "Login",
    component: LoginPage
  },
  {
    path: "/recuperarsenha",
    layout: "/auth",
    name: "RecuperarSenha",
    component: RecuperarSenhaPage
    
  }
];
export default login;